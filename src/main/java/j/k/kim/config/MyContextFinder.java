package j.k.kim.config;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AssignableTypeFilter;

import j.k.kim.http.filter.MyFilter;
import j.k.kim.http.servlet.MyServlet;

public class MyContextFinder {

	private static final Logger logger = LoggerFactory.getLogger(MyContextFinder.class);
	
	public static Map<String, MyServlet> findServlet(String packageName) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		return findMyType(packageName, MyServlet.class);
	}
	
	public static Map<String, MyFilter> findFilter(String packageName) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		return findMyType(packageName, MyFilter.class);
	}

	@SuppressWarnings("unchecked")
	public static <T> Map<String, T> findMyType(String packageName, Class<T> clazz) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		logger.info("MyContextFinder.findMyType[" + clazz.getName() + "][" + packageName + "]");
		Map<String, T> myType = new HashMap<>();

		ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false);

		scanner.addIncludeFilter(new AssignableTypeFilter(clazz));

		for (BeanDefinition bd : scanner.findCandidateComponents(packageName)) {
			String fullClassName = bd.getBeanClassName();
			Class<T> serviceClass = (Class<T>) Class.forName(fullClassName);
			T t = (T) serviceClass.newInstance();
			myType.put(fullClassName, t);
		}

		return myType;
	}
	
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		Map<String, MyServlet> servlets = findServlet("j.k.kim");
		servlets.entrySet().stream().forEach(mapItem -> System.out.println("servlet key[" + mapItem.getKey() + "]value[" + mapItem.getValue() + "]\n"));
		
		System.out.println("=================");
		Map<String, MyFilter> filters = findFilter("j.k.kim");
		filters.entrySet().stream().forEach(mapItem -> System.out.println("filter key[" + mapItem.getKey() + "]value[" + mapItem.getValue() + "]\n"));
	}
}
