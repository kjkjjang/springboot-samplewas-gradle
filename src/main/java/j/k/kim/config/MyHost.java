package j.k.kim.config;

import java.util.ArrayList;
import java.util.List;

public class MyHost {

	private String hostName;
	private String httpRoot;

	private String handle403ErrorPage;
	private String handle404ErrorPage;
	private String handle500ErrorPage;

	private List<String> filterList;
	
	public static List<MyHost> getDefaultConfig() {
		MyHost item = new MyHost();
		item.hostName = "kjk850.wendies.org";
		item.httpRoot = "/";
		
		item.handle403ErrorPage = "root_403.jsp";
		item.handle404ErrorPage = "root_404.jsp";
		item.handle500ErrorPage = "root_500.jsp";
		
		item.setFilterList(new ArrayList<>());
		
		List<MyHost> list = new ArrayList<>();
		list.add(item);
		return list;
	}

	public MyHost() {
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getHttpRoot() {
		return httpRoot;
	}

	public void setHttpRoot(String httpRoot) {
		this.httpRoot = httpRoot;
	}

	public String getHandle403ErrorPage() {
		return handle403ErrorPage;
	}

	public void setHandle403ErrorPage(String handle403ErrorPage) {
		this.handle403ErrorPage = handle403ErrorPage;
	}

	public String getHandle404ErrorPage() {
		return handle404ErrorPage;
	}

	public void setHandle404ErrorPage(String handle404ErrorPage) {
		this.handle404ErrorPage = handle404ErrorPage;
	}

	public String getHandle500ErrorPage() {
		return handle500ErrorPage;
	}

	public void setHandle500ErrorPage(String handle500ErrorPage) {
		this.handle500ErrorPage = handle500ErrorPage;

	}

	public List<String> getFilterList() {
		return filterList;
	}

	public void setFilterList(List<String> filterList) {
		this.filterList = filterList;
	}


	public String getErrorPage(int errorCode) {
		if (errorCode == 403) {
			return handle403ErrorPage;
		} else if (errorCode == 404) {
			return handle404ErrorPage;
		} else if (errorCode == 500) {
			return handle500ErrorPage;
		} else {
			return null;
		}
	}
	
	@Override
	public String toString() {
		return "hostname[" + hostName + "]httpRoot[" + httpRoot + "]"
				+ "403page[" + handle403ErrorPage + "]404page[" + handle404ErrorPage + "]500page[" + handle500ErrorPage + "]";
	}

}
