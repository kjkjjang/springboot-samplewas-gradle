package j.k.kim.config;

public class MyServletAlias {
	private String name;
	private String value;

	public MyServletAlias() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
