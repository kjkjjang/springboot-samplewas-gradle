package j.k.kim.config;

import java.util.List;
import java.util.stream.Collectors;

public class ServerConfig {

	private int port;
	private MyHost defaultHostInfo;

	private List<MyHost> hostInfo;
	private List<MyServletAlias> servletAlias;
	
	public static ServerConfig getDefaultConfig() {
		ServerConfig config = new ServerConfig();
		config.port = 8000;
		config.hostInfo = MyHost.getDefaultConfig();
		
		return config;
	}
	
	public ServerConfig() {
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public List<MyHost> getHostInfo() {
		return hostInfo;
	}

	public void setHostInfo(List<MyHost> hostInfo) {
		this.hostInfo = hostInfo;
	}

	public MyHost getDefaultHostInfo() {
		return defaultHostInfo;
	}

	public void setDefaultHostInfo(MyHost defaultHostInfo) {
		this.defaultHostInfo = defaultHostInfo;
	}

	public List<MyServletAlias> getServletAlias() {
		return servletAlias;
	}

	public void setServletAlias(List<MyServletAlias> servletAlias) {
		this.servletAlias = servletAlias;
	}

	
	@Override
	public String toString() {
		return  "port[" + port + "]\n"
				+ "defaultHost:" + defaultHostInfo + "\n"
				+ hostInfo.stream().map(host -> host.toString() + "\n").collect(Collectors.joining());
	}


}
