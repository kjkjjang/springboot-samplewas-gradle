package j.k.kim.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ServerConfigParser {

	private static final Logger logger = LoggerFactory.getLogger(ServerConfigParser.class);
	
	public static File stringFileNameToFile(String fileName) throws IOException {
		ClassPathResource resource = new ClassPathResource(fileName);
		if (!resource.exists()) {
			throw new FileNotFoundException(fileName);
		}
		return streamToFile(resource.getInputStream(), fileName);
	}
	
	public static File streamToFile(InputStream in, String fileName) throws IOException {
		String prefix, suffix;
		int index = -1;
		if (fileName.contains("/")) {
			index = fileName.lastIndexOf("/");
		} else if (fileName.contains(".") ) {
			index = fileName.lastIndexOf(".");
		} else {
		}
		if (index == -1) {
			return streamToFile(in, fileName, null);
		} else {
			prefix = fileName.substring(0, index);
			suffix = fileName.substring(index + 1);
			return streamToFile(in, prefix, suffix);			
		}

	}
	
	public static File streamToFile(InputStream in, String prefix, String suffix) throws IOException {
		final File tempFile = File.createTempFile(prefix, suffix);
		tempFile.deleteOnExit();
		try (FileOutputStream out = new FileOutputStream(tempFile)) {
			IOUtils.copy(in, out);
		}
		return tempFile;
	}
	
	public static ServerConfig serverConfigParser(String configFilePath) throws JsonParseException, JsonMappingException, IOException, URISyntaxException {
		boolean isFileExist = true;
		ClassPathResource resource = null;
		File confFile = null;
		if (configFilePath == null || configFilePath.trim().length() == 0 ) {
			String defaultFileName = "/j/k/kim/server_config.json";
			resource = new ClassPathResource(defaultFileName);
			if (!resource.exists()) {
				isFileExist = false;
			} else {
				confFile = streamToFile(resource.getInputStream(), defaultFileName);
			}
		} else {
			resource = new ClassPathResource(configFilePath);
			confFile = streamToFile(resource.getInputStream(), configFilePath);
		}
		logger.info("ServerConfigParser.serverConfigParser[" + configFilePath + "][" + confFile.getName() + "]");

		ServerConfig config = null;
		if (isFileExist) {
			ObjectMapper mapper = new ObjectMapper();
			config = mapper.readValue(confFile, ServerConfig.class);
		} else {
			config = ServerConfig.getDefaultConfig();
		}
		
		return config;
	}
	
}
