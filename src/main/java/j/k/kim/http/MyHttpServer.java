package j.k.kim.http;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import j.k.kim.config.ServerConfig;

public class MyHttpServer {
	
	private static final Logger logger = LoggerFactory.getLogger(MyHttpServer.class);
	private static final int NUM_THREADS = 50;
	
	private final int port;
	private ServerConfig config;

	public MyHttpServer(ServerConfig config) throws IOException {
		this.config = config;
		this.port = config.getPort();
	}

	public void start() throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException {
		ExecutorService pool = Executors.newFixedThreadPool(NUM_THREADS);
		try (ServerSocket server = new ServerSocket(port)) {
			logger.info("Accepting connections on port " + server.getLocalPort());
			while (true) {
				try {
					Socket socket = server.accept();
					Runnable r = new MyServerWorker(config, socket);
					pool.submit(r);
				} catch (IOException ex) {
					logger.warn("Error accepting connection", ex);
				}
			}
		}
	}

}
