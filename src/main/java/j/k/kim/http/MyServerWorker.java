package j.k.kim.http;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.Socket;
import java.nio.file.Files;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import j.k.kim.config.ServerConfig;
import j.k.kim.http.servlet.MyServlet;
import j.k.kim.request.MyRequest;
import j.k.kim.response.MyResponse;

public class MyServerWorker implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(MyServerWorker.class);
	
	private Socket connection;
	@SuppressWarnings("unused")
	private ClassPathResource resource;
	
	private MyServerWorkerProcessor processor;

	public MyServerWorker(ServerConfig config, Socket connection) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException {
		this.connection = connection;
		processor = new MyServerWorkerProcessor(config);
	}

	@Override
	public void run() {
		Writer out = null;
		BufferedReader in = null;
		OutputStream raw = null;
		try {
			in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
			raw = new BufferedOutputStream(connection.getOutputStream());
			out = new OutputStreamWriter(raw);
			
			MyRequest request = new MyRequest(in);
			MyResponse response = new MyResponse();
			MyServlet servlet = null;
			logger.info("Request\n[" + request + "]");
			
			// dont find host
			if (processor.indexOfHost(request) == -1) {
				logger.trace("Host Not Found\n[" + request + "]");
				processor.setResponsePage(404, response);
				sendResponse(response, out, raw);
				return;
				
			} else {
				processor.setHttpVersion(request, response);
				
				servlet = processor.getMyServlet(request);
				processor.callServletFilter(request, response, servlet);
				processor.generateResponsePage(response);

				logger.info("Response[" + response + "]");
				sendResponse(response, out, raw);
				
			}
			
		} catch (IOException ex) {
			logger.trace("Error talking to " + connection.getRemoteSocketAddress(), ex);
		} catch (MyServletException e) {
			MyResponse response = new MyResponse();

			response.getResponseLine().setReasonPhrase(e.getMessage());
			
			try {
				processor.setResponsePage(e.getErrorCode(), response);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
			logger.info("Error Response[" + response + "]");
			sendResponse(response, out, raw);
		} finally {
			try {
				connection.close();
			} catch (IOException ex) {
			}
		}
	}
	
	public void sendResponse(MyResponse response, Writer out, OutputStream raw) {
		try {
			String responseHeader = response.getHeader();
			out.write(responseHeader);
			out.flush();
			if (response.isBodyFile()) {
				byte[] bodyArr = Files.readAllBytes(response.getResponseFile().toPath());
				raw.write(bodyArr);	
			} else {
				out.write(response.getResponseBody());
			}
			
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
