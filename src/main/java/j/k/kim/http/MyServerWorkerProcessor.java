package j.k.kim.http;

import java.io.File;
import java.io.IOException;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

import j.k.kim.config.MyContextFinder;
import j.k.kim.config.MyHost;
import j.k.kim.config.MyServletAlias;
import j.k.kim.config.ServerConfig;
import j.k.kim.config.ServerConfigParser;
import j.k.kim.http.filter.MyFilter;
import j.k.kim.http.filter.MyFilterChain;
import j.k.kim.http.header.EntityHeader;
import j.k.kim.http.header.request.RequestHeader;
import j.k.kim.http.servlet.MyServlet;
import j.k.kim.http.servlet.NotGetMethodServlet;
import j.k.kim.http.servlet.PageNotFoundServlet;
import j.k.kim.request.MyRequest;
import j.k.kim.response.MyResponse;

public class MyServerWorkerProcessor {

	private ServerConfig config;
	private Map<String, MyServlet> servletMap;
	private Map<String, MyFilter> filterMap;
	private int configIndex;
	
	public MyServerWorkerProcessor(ServerConfig config) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		this.config = config;

		this.servletMap = MyContextFinder.findServlet("j.k.kim");
		this.filterMap = MyContextFinder.findFilter("j.k.kim");

		configIndex = -1;
	}

	public MyHost getMyHost() {
		if (configIndex == -1) {
			return config.getDefaultHostInfo();
		} else {
			return config.getHostInfo().get(configIndex);
		}
	}
	
	public int indexOfHost(MyRequest request) {
		for (int i = 0; i < config.getHostInfo().size(); i++) {
			String hostName = request.getHeaderValue(RequestHeader.Host.getCategory());
			if (hostName.contains(":")) {
				int indexOfPort = hostName.indexOf(":");
				hostName = hostName.substring(0, indexOfPort);
			}
			if (hostName.equals(config.getHostInfo().get(i).getHostName() ) ) {
				this.configIndex = i;
				break;
			}
		}
		return this.configIndex;
	}
	
	public void setHttpVersion(MyRequest request, MyResponse response) {
		if (request.requestLine.httpVersion.startsWith("HTTP")) {
			response.getResponseLine().setHttpVersion(request.requestLine.httpVersion);
		}
	}
	
	public MyServlet getMyServlet(MyRequest request) {
		MyServlet servlet = null;
		String servletName = null;
		if (request.requestLine.method.equals("GET")) {
			if (request.requestLine.requestUri.contains("/")) {
				if (request.requestLine.requestUri.equals("/")) {
					//index page called
					servlet = servletMap.get("j.k.kim.http.servlet.DefaultServlet");
				} else {
					// servlet page called
					int paramIndex = request.requestLine.requestUri.indexOf("?");
					if (paramIndex == -1) {
						servletName = request.requestLine.requestUri.trim().substring(1);	
					} else {
						servletName = request.requestLine.requestUri.trim().substring(1, paramIndex);
					}

					if (servletMap.containsKey(servletName)) {
						servlet = servletMap.get(servletName);
					} else {
						for (int i = 0; i < config.getServletAlias().size(); i++) {
							MyServletAlias alias = config.getServletAlias().get(i);
							if (alias.getName().equals(servletName)) {
								servlet = servletMap.get(config.getServletAlias().get(i).getValue());
								break;
							}
						}
						if (servlet == null) {
							servlet = new PageNotFoundServlet(getMyHost().getHandle404ErrorPage());
						}
					}
					
				}
			} else {
				// index page called
				servlet = servletMap.get("j.k.kim.http.servlet.DefaultServlet");
			}
		} else {
			// is not get method
			servlet = new NotGetMethodServlet();
		}
		
		return servlet;
	}
	
	public void callServletFilter(MyRequest request, MyResponse response, MyServlet servlet) {
		List<String> filterList = getMyHost().getFilterList();
		
		MyFilterChain chain = new MyFilterChain(servlet);
		for (String filter : filterList) {
			if (filterMap.containsKey(filter)) {
				chain.addFilter(filterMap.get(filter));
			}
			 
		}
		
		// filter chain and servlet call
		chain.doFilter(request, response);
	}

	public void generateResponsePage(MyResponse response) throws IOException {
		// response is file
		if (response.isBodyFile()) {
			File responseFile = ServerConfigParser.stringFileNameToFile(getMyHost().getHttpRoot() + "/" + response.getResponseFileName());
			if (responseFile.exists() && responseFile.canRead()) {
				// set response body
				setResponsePage(200, response.getResponseFileName(), response);
			} else {
				setResponsePage(404, getMyHost().getHandle404ErrorPage(), response);
			}
		} else {
			// response is text
			setResponseText(200, response);
		}
	}
	
	public void setResponseText(int statusCode, MyResponse response) {
		response.setHeader(EntityHeader.ContentType.getCategory(), "text/html; charset=UTF-8");
		response.getResponseLine().setHttpVersion("HTTP/1.1");
		response.getResponseLine().setStatusCode(statusCode);
	}
	
	public void setResponsePage(int statusCode, MyResponse response) throws IOException {
		File responseFile = ServerConfigParser.stringFileNameToFile(getMyHost().getHttpRoot() + "/" + getMyHost().getErrorPage(statusCode));
		
		response.setHeader(EntityHeader.ContentType.getCategory(), 
				URLConnection.getFileNameMap().getContentTypeFor(responseFile.getName()));
		// request file is Not exist
		// can't find the file
		response.getResponseLine().setHttpVersion("HTTP/1.1");
		response.getResponseLine().setStatusCode(statusCode);
		response.setResponseFile(responseFile);

	}
	
	public void setResponsePage(int statusCode, String responseFileName, MyResponse response) throws IOException {
		File responseFile = ServerConfigParser.stringFileNameToFile(getMyHost().getHttpRoot() + "/" + responseFileName);
		
		response.setHeader(EntityHeader.ContentType.getCategory(), 
				URLConnection.getFileNameMap().getContentTypeFor(responseFile.getName()));
		// request file is Not exist
		// can't find the file
		response.getResponseLine().setHttpVersion("HTTP/1.1");
		response.getResponseLine().setStatusCode(statusCode);
		response.setResponseFile(responseFile);

	}
}
