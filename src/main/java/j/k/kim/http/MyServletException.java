package j.k.kim.http;

public class MyServletException extends RuntimeException {

	private static final long serialVersionUID = 9187097765561036957L;

	private String message;

	private int errorCode;
	
	private String errorPage;
	
	public MyServletException() {
	}
	
	public MyServletException(int errorCode, String message, String errorPage) {
		this.errorCode = errorCode;
		this.message = message;
		this.errorPage = errorPage;
	}
	
	public String getMessage() {
		return message;
	}
	
	public int getErrorCode() {
		return errorCode;
	}
	
	public String getErrorPage() {
		return errorPage;
	}
}
