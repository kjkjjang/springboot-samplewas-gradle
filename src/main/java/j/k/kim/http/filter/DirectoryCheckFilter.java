package j.k.kim.http.filter;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import j.k.kim.http.MyServletException;
import j.k.kim.request.MyRequest;
import j.k.kim.response.MyResponse;

public class DirectoryCheckFilter implements MyFilter {

	private static final Logger logger = LoggerFactory.getLogger(DirectoryCheckFilter.class);
	
	private Pattern pattern;
	private List<String> patternList;
	
	public DirectoryCheckFilter() {
		this.patternList = Arrays.asList("\\.\\.");
	}
	
	public DirectoryCheckFilter(List<String> patternList) {
		this.patternList = patternList;
	}

	public void setPatternList(List<String> patternList) {
		this.patternList = patternList;
	}
	
	@Override
	public void doFilter(MyRequest request, MyResponse response, MyFilterChain chain) {
		logger.info("DirectoryCheckFilter called");

		// before request
		for (String checkString : patternList) {
			// Create a Pattern object
			pattern = Pattern.compile(checkString);

			// Now create matcher object.
			Matcher m = pattern.matcher(request.requestLine.requestUri);
			// uri . 포함 되는 예외 처리
			if (m.find()) {
				logger.error("Filter find exception pattern[" + checkString + "]");
				throw new MyServletException(500, "request URI에 [" + checkString + "] 요청 될 수 없습니다.", "_500.html");
			}
		}
		
		// chain called
		if (chain != null) {
			chain.doFilter(request, response);
		}
		
		// after request
		
	}
	
}
