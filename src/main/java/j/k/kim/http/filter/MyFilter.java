package j.k.kim.http.filter;

import j.k.kim.request.MyRequest;
import j.k.kim.response.MyResponse;

public interface MyFilter {

	public void doFilter(MyRequest request, MyResponse response, MyFilterChain chain);

}
