package j.k.kim.http.filter;

import java.util.ArrayList;
import java.util.List;

import j.k.kim.http.servlet.MyServlet;
import j.k.kim.request.MyRequest;
import j.k.kim.response.MyResponse;

public class MyFilterChain {

	private List<MyFilter> filterList;
	private MyServlet servlet;
	
	public MyFilterChain(MyServlet servlet) {
		filterList = new ArrayList<>();
		this.servlet = servlet;
	}

	public MyFilterChain(List<MyFilter> filterList, MyServlet servlet) {
		this.filterList = filterList;
		this.servlet = servlet;
	}

	public void addFilter(MyFilter chain) {
		filterList.add(chain);
	}

	public void doFilter(MyRequest request, MyResponse response) {
		if (filterList.size() > 0) {
			MyFilterChain nextChain = new MyFilterChain(filterList.subList(1, filterList.size()), servlet);
			filterList.get(0).doFilter(request, response, nextChain);
		} else {
			if (servlet != null) {
				servlet.serve(request, response);	
			}
			
		}
	}

}
