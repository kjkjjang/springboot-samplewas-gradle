package j.k.kim.http.header;

/*
   entity-header  = Allow                    ; Section 14.7
                  | Content-Encoding         ; Section 14.11
                  | Content-Language         ; Section 14.12
                  | Content-Length           ; Section 14.13
                  | Content-Location         ; Section 14.14
                  | Content-MD5              ; Section 14.15
                  | Content-Range            ; Section 14.16
                  | Content-Type             ; Section 14.17
                  | Expires                  ; Section 14.21
                  | Last-Modified            ; Section 14.29
                  | extension-header
 */
public enum EntityHeader implements MyHeader {
	Allow("Allow"),
	ContentEncoding("Content-Encoding"),
	ContentLanguage("Content-Language"),
	ContentLength("Content-Length"),
	ContentLocation("Content-Location"),
	ContentMD5("Content-MD5"),
	ContentRange("Content-Range"),
	ContentType("Content-Type"),
	Expires("Expires"),
	LastModified("Last-Modified");
	
	private String category;
	
	EntityHeader(String category) {
		this.category = category;
	}
	
	public String getCategory() {
		return category;
	}
	
	public static boolean contains(String category) {
		for(EntityHeader header : values()) {
			if (header.category.equals(category)) {
				return true;
			}
		}
		return false;
	}
}
