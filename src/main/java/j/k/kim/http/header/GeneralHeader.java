package j.k.kim.http.header;

/*
general-header = Cache-Control            ; Section 14.9
               | Connection               ; Section 14.10
               | Date                     ; Section 14.18
               | Pragma                   ; Section 14.32
               | Trailer                  ; Section 14.40
               | Transfer-Encoding        ; Section 14.41
               | Upgrade                  ; Section 14.42
               | Via                      ; Section 14.45
               | Warning                  ; Section 14.46 
 */
public enum GeneralHeader implements MyHeader {
	Cache("Cache-Control"),
	Connection("Connection"),
	Date("Date"),
	Pragma("Pragma"),
	Trailer("Trailer"),
	Transfer("Transfer-Encoding"),
	Upgrade("Upgrade"),
	Via("Via"),
	Warning("Warning");

	private String category;
	
	GeneralHeader(String category) {
		this.category = category;
	}
	
	public String getCategory() {
		return category;
	}
	
	public static boolean contains(String category) {
		for(GeneralHeader header : values()) {
			if (header.category.equals(category)) {
				return true;
			}
		}
		return false;
	}
	
	public int indexOf(String category) {
		for(int i = 0; i < values().length; i++) {
			if (values()[i].category.equals(category)) {
				return i;
			}
		}
		return -1;
	}
}
