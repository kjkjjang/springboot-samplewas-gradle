package j.k.kim.http.header.request;

import j.k.kim.http.header.MyHeader;

/*
request-header = Accept                   ; Section 14.1
               | Accept-Charset           ; Section 14.2
               | Accept-Encoding          ; Section 14.3
               | Accept-Language          ; Section 14.4
               | Authorization            ; Section 14.8
               | Expect                   ; Section 14.20
               | From                     ; Section 14.22
               | Host                     ; Section 14.23
               | If-Match                 ; Section 14.24
               | If-Modified-Since        ; Section 14.25
               | If-None-Match            ; Section 14.26
               | If-Range                 ; Section 14.27
               | If-Unmodified-Since      ; Section 14.28
               | Max-Forwards             ; Section 14.31
               | Proxy-Authorization      ; Section 14.34
               | Range                    ; Section 14.35
               | Referer                  ; Section 14.36
               | TE                       ; Section 14.39
               | User-Agent               ; Section 14.43
*/
public enum RequestHeader implements MyHeader {
	Accept("Accept"),
	AcceptCharset("Accept-Charset"),
	AcceptEncoding("Accept-Encoding"),
	AcceptLanguage("Accept-Language"),
	Authorization("Authorization"),
	Expect("Expect"),
	From("From"),
	Host("Host"),
	IfMatch("If-Match"),
	IfModifiedSince("If-Modified-Since"),
	IfNoneMatch("If-None-Match"),
	IfRange("If-Range"),
	IfUnmodifiedSince("If-Unmodified-Since"),
	MaxForwards("Max-Forwards"),
	ProxyAuthorization("Proxy-Authorization"),
	Range("Range"),
	Referer("Referer");
	
	private String category;
	
	RequestHeader(String category) {
		this.category = category;
	}
	
	public String getCategory() {
		return category;
	}
	
	public static boolean contains(String category) {
		for(RequestHeader header : values()) {
			if (header.category.equals(category)) {
				return true;
			}
		}
		return false;
	}
}
