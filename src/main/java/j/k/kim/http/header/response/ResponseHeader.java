package j.k.kim.http.header.response;

/*
	   response-header = Accept-Ranges       ; Section 14.5
       | Age                     ; Section 14.6
       | ETag                    ; Section 14.19
       | Location                ; Section 14.30
       | Proxy-Authenticate      ; Section 14.33

       | Retry-After             ; Section 14.37
       | Server                  ; Section 14.38
       | Vary                    ; Section 14.44
       | WWW-Authenticate        ; Section 14.47
 */
public enum ResponseHeader {

	Age("Age"),
	ETag("ETag"),
	Location("Location"),
	ProxyAuthenticate("Proxy-Authenticate"),
	RetryAfter("Retry-After"),
	Server("Server"),
	Vary("Vary"),
	WWWAuthenticate("WWW-Authenticate");
	
	private String category;
	
	private ResponseHeader(String category) {
		this.category = category;
	}
	
	public static boolean contains(String category) {
		for(ResponseHeader header : values()) {
			if (header.category.equals(category)) {
				return true;
			}
		}
		return false;
	}
}
