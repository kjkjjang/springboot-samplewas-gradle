package j.k.kim.http.servlet;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import j.k.kim.request.MyRequest;
import j.k.kim.response.MyResponse;
import j.k.kim.response.MyResponseWriter;

public class DateServlet extends DefaultServlet {

	private static final Logger logger = LoggerFactory.getLogger(DateServlet.class);
	
	@Override
	public void serve(MyRequest request, MyResponse response) {
		logger.info("DateServlet called");
		
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		MyResponseWriter writer = response.getWriter();
		writer.writeLine("now[" + now.format(formatter) + "]");
		writer.writeLine("Date[" + now + "]");
	}
	
	@Override
	public String toString() {
		return "date Servlet toString";
	}
}
