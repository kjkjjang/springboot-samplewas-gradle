package j.k.kim.http.servlet;

import j.k.kim.request.MyRequest;
import j.k.kim.response.MyResponse;

/**
 * 	servlet class 정의용 annotation
 */
public interface MyServlet {

	public void serve(MyRequest request, MyResponse response);
}
