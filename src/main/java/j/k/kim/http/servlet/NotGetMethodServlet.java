package j.k.kim.http.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import j.k.kim.request.MyRequest;
import j.k.kim.response.MyResponse;

public class NotGetMethodServlet implements MyServlet {

	private static final Logger logger = LoggerFactory.getLogger(NotGetMethodServlet.class);
	
	public NotGetMethodServlet() {
	}
	
	@Override
	public void serve(MyRequest request, MyResponse response) {
		logger.info(getClass() + " called");
		
		response.getResponseLine().setStatusCode(501);
		response.getResponseLine().setHttpVersion("HTTP/1.1");
		response.getResponseLine().setReasonPhrase("Not Implemented");

		StringBuffer sb  = new StringBuffer();
		
		sb.append("<HTML>\r\n");
		sb.append("<HEAD><TITLE>Not Implemented</TITLE>\r\n");
		sb.append("</HEAD>\r\n");
		sb.append("<BODY>");
		sb.append("<H1>HTTP Error 501: Not Implemented</H1>\r\n");
		sb.append("</BODY></HTML>\r\n");

		
		response.setResponseBody(sb.toString());

	}

}
