package j.k.kim.http.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import j.k.kim.request.MyRequest;
import j.k.kim.response.MyResponse;

public class PageNotFoundServlet implements MyServlet {

	private static final Logger logger = LoggerFactory.getLogger(PageNotFoundServlet.class);
	
	private String errorPage;
	
	public PageNotFoundServlet() {
	}
	
	public PageNotFoundServlet(String errorPage) {
		this.errorPage = errorPage;
	}
	
	@Override
	public void serve(MyRequest request, MyResponse response) {
		logger.info(getClass() + " called");
		
		response.setResponseFileName(errorPage);
	}
}
