package j.k.kim.http.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import j.k.kim.request.MyRequest;
import j.k.kim.response.MyResponse;

public class PrintServlet extends DefaultServlet {
	
	private static final Logger logger = LoggerFactory.getLogger(PageNotFoundServlet.class);

	@Override
	public void serve(MyRequest request, MyResponse response) {
		logger.info(getClass() + " called");
		response.setResponseFileName("print_servlet.html");
	}
	
	@Override
	public String toString() {
		return "print Servlet toString";
	}
}
