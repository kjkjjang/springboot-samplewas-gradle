package j.k.kim.request;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import j.k.kim.http.MyServletException;
import j.k.kim.http.header.EntityHeader;
import j.k.kim.http.header.GeneralHeader;
import j.k.kim.http.header.request.RequestHeader;

/*
 * from  https://www.w3.org/Protocols/rfc2616/rfc2616-sec5.html

 
Request       = Request-Line              ; Section 5.1
                *(( general-header        ; Section 4.5
                 | request-header         ; Section 5.3
                 | entity-header ) CRLF)  ; Section 7.1
                CRLF
                [ message-body ]          ; Section 4.3
 */
public class MyRequest {

	public MyRequestLine requestLine;

	public Map<String, String> generalHeader;

	public Map<String, String> requestHeader;
	
	public Map<String, String> entityHeader;

	public Map<String, Object> parameter;
	
	public StringBuffer requestBody;
	
	public MyRequest(String requestString) throws IOException {
		parseString(requestString);
	}

	public MyRequest(BufferedReader in) throws IOException {
		StringBuilder requestString = new StringBuilder();
		String requestLine;

		try {
			while(( requestLine = in.readLine()) != null) {
				if (requestLine != null && requestLine.trim().length() > 0) {
					requestString.append(requestLine + "\n");	
				}
				if (requestLine.length() == 0) {
					break;
				}
			}
		} catch (IOException e) {
			throw new MyServletException(400, "Socket IO Exception[" + e.getMessage() + "]", "_400.html");
		}

		parseString(requestString.toString());
	}

	private void parseString(String requestString) throws IOException {
		BufferedReader reader = new BufferedReader(new StringReader(requestString));

		requestLine = new MyRequestLine(reader.readLine());

		if (entityHeader == null) {
			entityHeader = new HashMap<>();
		}
		if (requestHeader == null) {
			requestHeader = new HashMap<>();
		}
		if (generalHeader == null) {
			generalHeader = new HashMap<>();
		}

		
		String header = reader.readLine();
		while (header != null) {
			if (header.length() > 0) {
				setRequestHeader(header);
			}
			header = reader.readLine();
		}
		
		String bodyLine = reader.readLine();
		while (bodyLine != null) {
			if (bodyLine.length() > 0) {
				appendMessageBody(bodyLine);	
			}
			bodyLine = reader.readLine();
		}
	
		reader.close();
		
		
		if (parameter == null) {
			parameter = new HashMap<>();	
		}
		
		if (requestLine.requestUri.contains("?")) {
			String[] uriSplit = requestLine.requestUri.split("\\?");
			if (uriSplit.length > 1 && uriSplit[1].length() > 0) {
				String paramString = requestLine.requestUri.split("\\?")[1];
				String[] paramArray = paramString.split("&");
				for (String keyValueString : paramArray) {
					String[] keyValue = keyValueString.split("=");
					parameter.put(keyValue[0], keyValue[1]);	
				}
			}
			
		}


	}
	
	private void setRequestHeader(String headerString) {
		int splitIndex = headerString.indexOf(":");
		String key = headerString.substring(0, splitIndex);
		String value = headerString.substring(splitIndex + 1);
		
		boolean isGeneralCategory = GeneralHeader.contains(key);
		boolean isRequestCategory = RequestHeader.contains(key);
		boolean isEntityCategory = EntityHeader.contains(key);
		
		if (isGeneralCategory) {
			generalHeader.put(key, value.trim());
		}
		if (isRequestCategory) {
			requestHeader.put(key, value.trim());
		}
		if (isEntityCategory) {
			entityHeader.put(key, value.trim());
		}
	}

	private void appendMessageBody(String bodyString) {
		if (requestBody == null) {
			requestBody = new StringBuffer();
		}
		this.requestBody.append(bodyString);
	}

	public String getHeaderValue(String category) {
		boolean isGeneralCategory = GeneralHeader.contains(category);
		boolean isRequestCategory = RequestHeader.contains(category);
		boolean isEntityCategory = EntityHeader.contains(category);
		
		String value = null;
		if (isGeneralCategory) {
			value = generalHeader.get(category);
		}
		if (isRequestCategory) {
			value = requestHeader.get(category);
		}
		if (isEntityCategory) {
			value = entityHeader.get(category);
		}
		return value;
	}
	
	public Object getParameter(String key) {
		if (parameter.containsKey(key) ) {
			return parameter.get(key);
		}
		return null;
	}
	
	public String getRequestBody() {
		if (requestBody == null) {
			return "";
		} else {
			return requestBody.toString();
		}
	}
	
	@Override
	public String toString() {
		return this.requestLine 
				+ this.generalHeader.entrySet().stream().map(keyValue -> keyValue.getKey() + ":" + keyValue.getValue() + "\r\n").collect(Collectors.joining())
				+ this.requestHeader.entrySet().stream().map(keyValue -> keyValue.getKey() + ":" + keyValue.getValue() + "\r\n").collect(Collectors.joining())
				+ this.entityHeader.entrySet().stream().map(keyValue -> keyValue.getKey() + ":" + keyValue.getValue() + "\r\n").collect(Collectors.joining())
				+ this.parameter.entrySet().stream().map(keyValue -> keyValue.getKey() + ":" + keyValue.getValue() + "\r\n").collect(Collectors.joining())
				+ "\r\n" + this.requestBody
				;
	}

}
