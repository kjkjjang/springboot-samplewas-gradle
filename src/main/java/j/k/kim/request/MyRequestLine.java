package j.k.kim.request;

import j.k.kim.http.MyServletException;

/*
 * Request-Line = Method SP Request-URI SP HTTP-Version CRLF
					ex) GET / HTTP/1.1
 */
public class MyRequestLine {
	/*
	 Method         = "OPTIONS"                ; Section 9.2
                     | "GET"                    ; Section 9.3
                     | "HEAD"                   ; Section 9.4
                     | "POST"                   ; Section 9.5
                     | "PUT"                    ; Section 9.6
                     | "DELETE"                 ; Section 9.7
                     | "TRACE"                  ; Section 9.8
                     | "CONNECT"                ; Section 9.9
                     | extension-method
      extension-method = token
	 */
	public String method;
	
	/*
	 Request-URI    = "*" | absoluteURI | abs_path | authority
	 all HTTP/1.1 servers MUST accept the absoluteURI form in requests, even though HTTP/1.1 clients will only generate them in requests to proxies
	 the absolute path cannot be empty; if none is present in the original URI, it MUST be given as "/"
	 
	1. If Request-URI is an absoluteURI, the host is part of the Request-URI. Any Host header field value in the request MUST be ignored.
	2. If the Request-URI is not an absoluteURI, and the request includes a Host header field, the host is determined by the Host header field value.
	3. If the host as determined by rule 1 or 2 is not a valid host on the server, the response MUST be a 400 (Bad Request) error message.
	 */
	public String requestUri;
	
	
	public String httpVersion;
	
	public MyRequestLine(String requestLine) {
		String[] requestLineArr = requestLine.split("\\s+");
		if (requestLineArr.length != 3) {
			throw new MyServletException(400, "Request Line must 3 size", "_400.html");
		}
		this.method = requestLineArr[0];
		this.requestUri = requestLineArr[1];
		this.httpVersion = requestLineArr[2];
	}

	@Override
	public String toString() {
		return method + " " + requestUri + " " + httpVersion + "\r\n";
	}
}
