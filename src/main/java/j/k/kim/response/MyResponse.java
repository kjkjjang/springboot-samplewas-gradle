package j.k.kim.response;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import j.k.kim.http.header.EntityHeader;
import j.k.kim.http.header.GeneralHeader;
import j.k.kim.http.header.request.RequestHeader;

/*

https://www.w3.org/Protocols/rfc2616/rfc2616-sec6.html#sec6

  Response      = Status-Line               ; Section 6.1
                   *(( general-header        ; Section 4.5
                    | response-header        ; Section 6.2
                    | entity-header ) CRLF)  ; Section 7.1
                   CRLF
                   [ message-body ]          ; Section 7.2
                   
 */
public class MyResponse {

	private MyResponseLine responseLine;

	private Map<String, Object> generalHeader;

	private Map<String, Object> responseHeader;
	
	private Map<String, Object> entityHeader;

	private String responseBody;

	private File responseBodyFile;
	
	private String responseFileName;
	
	private MyResponseWriter responseBodyWriter;
	
	public MyResponse() {
		init();
	}
	
	private void init() {
		
		responseLine = new MyResponseLine();

		generalHeader = new HashMap<>();
		responseHeader = new HashMap<>();
		entityHeader = new HashMap<>();
		
		responseBodyWriter = new MyResponseWriter();
	}
	
	public void setHeader(String key, Object value) {
		boolean isGeneralCategory = GeneralHeader.contains(key);
		boolean isRequestCategory = RequestHeader.contains(key);
		boolean isEntityCategory = EntityHeader.contains(key);
		
		if (isGeneralCategory) {
			generalHeader.put(key, value);
		}
		if (isRequestCategory) {
			responseHeader.put(key, value);
		}
		if (isEntityCategory) {
			entityHeader.put(key, value);
		}

	}

	public void setResponseBody(String responseBody) {
		this.responseBody = responseBody;
	}
	
	public void setResponseFile(File responseFile) {
		this.responseBodyFile = responseFile;
	}

	public MyResponseLine getResponseLine() {
		return this.responseLine;
	}
	
	public String getHeader() {
		setHeader(GeneralHeader.Date.getCategory(), new Date());
		if (isBodyFile()) {
			try {
				byte[] fileArr = Files.readAllBytes(responseBodyFile.toPath());
				setHeader(EntityHeader.ContentLength.getCategory(), fileArr.length);	
			} catch (IOException e) {
			}
		} else {
			setHeader(EntityHeader.ContentLength.getCategory(), getResponseBody().length());
		}
		
		StringBuffer sb = new StringBuffer();
		sb.append(this.responseLine);
		sb.append(this.generalHeader.entrySet().stream().map(keyValue -> keyValue.getKey() + ":" + keyValue.getValue() + "\n").collect(Collectors.joining()));
		sb.append(this.responseHeader.entrySet().stream().map(keyValue -> keyValue.getKey() + ":" + keyValue.getValue() + "\n").collect(Collectors.joining()));
		sb.append(this.entityHeader.entrySet().stream().map(keyValue -> keyValue.getKey() + ":" + keyValue.getValue() + "\n").collect(Collectors.joining()));
		sb.append("\n");
		return sb.toString();
	}
	
	public boolean isBodyFile() {
		if (responseBodyFile != null || responseFileName != null) {
			return true;
		} else {
			return false;
		}
	}
	
	public File getResponseFile() {
		return this.responseBodyFile;
	}
	
	public String getResponseBody() {
		if (responseBodyWriter.isUsed()) {
			this.responseBody = responseBodyWriter.toString();
		}
		return this.responseBody;
	}

	public void setResponseFileName(String responseFileName) {
		this.responseFileName = responseFileName;
	}
	
	public String getResponseFileName() {
		return responseFileName;
	}
	
	public MyResponseWriter getWriter() {
		return responseBodyWriter;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(responseLine);
		sb.append(generalHeader.entrySet().stream().map(keyValue -> keyValue.getKey() + ":" + keyValue.getValue() + "\n").collect(Collectors.joining()));
		sb.append(responseHeader.entrySet().stream().map(keyValue -> keyValue.getKey() + ":" + keyValue.getValue() + "\n").collect(Collectors.joining()));
		sb.append(entityHeader.entrySet().stream().map(keyValue -> keyValue.getKey() + ":" + keyValue.getValue() + "\n").collect(Collectors.joining()));
		sb.append("\n");
		
		if (responseBody != null) {
			sb.append(responseBody);
		} else if (responseBodyFile != null) {
			sb.append(responseBodyFile.getName());
		} else if (responseBodyWriter.isUsed()) {
			sb.append(responseBodyWriter);
		} else {
			sb.append("\n");
		}
		sb.append("\n");
		return sb.toString();
	}
}
