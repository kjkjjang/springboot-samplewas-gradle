package j.k.kim.response;

public class MyResponseWriter {

	private StringBuffer stringBuffer;
	
	public MyResponseWriter() {
	}
	
	private void initBuffer() {
		if (stringBuffer == null) {
			stringBuffer = new StringBuffer();
		}
	}
	
	public boolean isUsed() {
		return stringBuffer != null;
	}
	
	public void write(Object object) {
		if (object != null) {
			initBuffer();
			stringBuffer.append(object.toString());
		}
	}
	
	public void writeLine(Object object) {
		if (object != null) {
			initBuffer();
			stringBuffer.append(object.toString() + "\n<br>");
		}
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("<HTML>\r\n");
		sb.append("<HEAD><TITLE>text page</TITLE>\r\n");
		sb.append("</HEAD>\r\n");
		sb.append("<BODY>");
		if (stringBuffer != null) {
			sb.append(stringBuffer);
		}
		sb.append("</BODY>\r\n</HTML>\r\n");
		
		return sb.toString();
	}
}
